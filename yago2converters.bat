@echo off
REM	 Adjust these parameters
REM MEMORY corresponds to the Java Xmx parameter, use to increase main memory
SET MEMORY=1G

REM DO NOT ADJUST BELOW HERE
SETLOCAL ENABLEEXTENSIONS
SETLOCAL ENABLEDELAYEDEXPANSION

SET MAIN_CLASS=converters.Converter	

SET CPLA=
REM create classpath - iterate over all subdirectories inside $LIB and add the jar files
for %%G in (lib\*.jar) do SET CPLA=!CPLA!;%%G

java -cp %CPLA% -Xmx%MEMORY% %MAIN_CLASS%
