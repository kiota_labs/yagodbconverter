#!/bin/sh

# Adjust these parameters
# MEMORY corresponds to the Java Xmx parameter, use to increase main memory
MEMORY=4G

# DO NOT ADJUST BELOW HERE
LIB=./lib                  			# external libraries
MAIN_CLASS=basics.QueryProcessor	# class whose main method should be run

# create classpath - iterate over all subdirectories inside $LIB and add the jar files
for jarf in $LIB/*.jar
do
  CLPA=$CLPA:$jarf
done

#run
echo Starting YAGO2 Converters

java -cp $CLPA -Xmx$MEMORY $MAIN_CLASS