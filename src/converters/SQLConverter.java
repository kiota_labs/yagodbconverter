package converters;

import java.io.File;
import java.io.IOException;
import java.sql.Types;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.io.FileUtils;
import javatools.administrative.Announce;
import javatools.administrative.Parameters;
import javatools.database.Database;
import javatools.administrative.D;
import javatools.database.Database.Inserter;
import javatools.database.PostgresDatabase;
import javatools.filehandlers.FileLines;
import basics.Config;
import org.apache.commons.io.filefilter.TrueFileFilter;


public class SQLConverter extends Converter {

  /** Holds the database */
  protected Database database = null;

  /** Table name */
  protected String factsTable = null;

  /** Special add-parameter for makeSQL */
  protected static final String INDEX = "INDEX";

  /** Adds facts from one file */
  public void loadFactsFrom(File file, String relation) throws Exception {
    //String relation = relationForFactFile(file);
    D.println ("relation name: ", relation);
    if (file.isDirectory()) return;

    int[] columnTypes = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };

    Inserter bulki = database.new Inserter(factsTable, columnTypes);
    int counter = 0;
    for (String l : new FileLines(file, "Parsing " + file)) {
      if (test && counter++ > 100) break;
      String[] split = l.split("\t");
      String id = split[0];
      String arg1 = split[1];
      String arg2 = split[2];
      if (relation.equals("hasGloss") && arg2.length() > maximumFactLength) {
        arg2 = arg2.subSequence(0, maximumFactLength - 4) + "...\"";
      } else if (arg1.length() > maximumFactLength || arg2.length() > maximumFactLength) {
        continue;
      }
      try {
        bulki.insert(id, relation, arg1, arg2);
      } catch (Exception e) {
        System.err.println(e);
        return;
      }
    }
    bulki.close();
  }

  @Override
  public void getParameters() throws IOException {
    super.getParameters();
    getExtendedParameters();
  }

  @Override
  public void run() throws Exception {
    getParameters();
    database = Config.getDatabase();
    Announce.doing("Writing to database");

    factsTable = Parameters.get("databaseTable", "facts");

    boolean createTable = Parameters.getBoolean("createFactsTable", true);

    // Create facts table
    if (createTable) {
      Announce.doing("Creating " + factsTable + " table");
      if (database instanceof PostgresDatabase) {
        database.executeUpdate("CREATE TABLE " + factsTable + " (id VARCHAR, relation VARCHAR, arg1 TEXT, arg2 TEXT)");
      } else {
        database.executeUpdate("CREATE TABLE " + factsTable + " (id VARCHAR(255), relation VARCHAR(255), arg1 VARCHAR(255), arg2 VARCHAR(255))");
      }
      Announce.done();
    }

    Announce.doing("can we get some reaction please?");
    //println("can we get some reaction please?");
    String relation = "";
    //for (File f : yagoFolder.listFiles()) {
    Iterator<File> files = FileUtils.iterateFiles(yagoFolder, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
    while (files.hasNext()) {
      File f = files.next();
      D.println("filename being parsed: ");
//      if (f.isDirectory()) {
//        relation = f.getName();
//      }
      relation = f.getParentFile().getName();
      loadFactsFrom(f, relation);
    }
    Announce.done();

    Announce.doing("Creating indices on " + factsTable);
    List<String[]> indexes = null;
        
    /* int indexColumnCount = Parameters.getInt("indexColumnCount", 2);

    switch (indexColumnCount) {
      case 1:
        indexes = Arrays.asList(
            new String[] { "id" }, 
            new String[] { "id", "arg1" },
            new String[] { "relation" }, 
            new String[] { "arg1" }, 
            new String[] { "arg2" });
        break;

      case 2:
        indexes = Arrays.asList(
            new String[] { "id" }, 
            new String[] { "id", "arg1" },
            new String[] { "relation", "arg1" }, 
            new String[] { "arg1", "relation" }, 
            new String[] { "arg2", "relation" },
            new String[] { "arg1", "arg2" },
            new String[] { "relation", "arg2" }, 
            new String[] { "arg2", "arg1" });
        break;
        
      case 3:
        indexes = Arrays.asList(new String[] { "id" },
            new String[] { "id", "arg1" },
            new String[] { "relation", "arg1", "arg2" }, 
            new String[] { "arg1", "relation", "arg2" }, 
            new String[] { "arg2", "relation", "arg1" },
            new String[] { "arg1", "arg2", "relation" },
            new String[] { "relation", "arg2", "arg1" }, 
            new String[] { "arg2", "arg1", "relation" });
        break;
        
      default: // no indexes
        indexes = Arrays.asList(new String[] { "id" },
            new String[] { "id", "arg1" } );
        break;
      } */

      indexes = Arrays.asList(new String[] { "id" },
            new String[] { "id", "arg1" } );

    // For large tables, MySQL uses a slow index building procedure, thus you can 
    // restrict the INDEX size by uncommenting the following. Otherwise see the 
    // README file for hints at MySQL index generation speedup or ask
    // Steffen Mezger for a way to build the usual indexes quickly
//    if (database instanceof MySQLDatabase) {
//      for (String[] l : indexes) {
//        for (int i = 0; i < l.length; i++)
//          l[i] += "(160)";
//      }
//    }
    
    if (database instanceof PostgresDatabase) {
      Announce.message("Statistics on the "+factsTable+" table are gathered");
      database.executeUpdate("ALTER TABLE "+factsTable+" ALTER id SET STATISTICS 1000");
      database.executeUpdate("ALTER TABLE "+factsTable+" ALTER relation SET STATISTICS 1000");
      database.executeUpdate("ALTER TABLE "+factsTable+" ALTER arg1 SET STATISTICS 1000");
      database.executeUpdate("ALTER TABLE "+factsTable+" ALTER arg2 SET STATISTICS 1000");
      database.executeUpdate("VACUUM ANALYZE "+factsTable+"");
    }
    Announce.done();
    
    Announce.message("The following indexes will be created:");
    for (String[] attr : indexes)
      Announce.message(database.createIndexCommand(factsTable, false, attr));
    Announce.message("On some systems, this fails. In these cases, please interrupt and create the indexes manually.");
    for (String[] attr : indexes) {
      Announce.doing(database.createIndexCommand(factsTable, false, attr));
      database.createIndex(factsTable, false, attr);
      Announce.done();
    }
    database.close();

    Announce.done();
  }

  @Override
  public String description() {
    return ("Read YAGO into a database\n      ----> use this option for SQL or native querying");
  }

  public static void main(String[] args) throws Exception {
    File iniFile = new File(args == null || args.length == 0 ? "yago.ini" : args[0]);
    Config.init(iniFile);
    new SQLConverter().run();
  }
}