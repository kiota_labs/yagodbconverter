               The YAGO Converters
                Aditya Khanna
               Kiota Labs Pvt Ltd
             This version: 2017-03-08

------------------ Introduction ---------------------------------------

This ZIP file contains the Java-programs to convert YAGO to different
formats.

This code is licensed under the Creative Commons Attribution License
(http://creativecommons.org/licenses/by/3.0/) by the YAGO team
(http://yago-knowledge.org). It contains the Jena and the Berkeley DB Java library.
See below for the licenses.

YAGO has been developed at MPI. Citing the paper that introduced YAGO -
    "YAGO - A Core of Semantic Knowledge"
    16th international World Wide Web conference (WWW 2007)
    PDF: http://www.mpii.de/~suchanek/publications/www2007.pdf
    BIB: http://www.mpii.de/~suchanek/publications/www2007.bib


------------------ What you need --------------------------------------

Java
IntelliJ Idea
Postgres up and running

------------------ Instructions ------------------------------------

[Optional] : Remove yago 2s zip file and folder to create space.

Dowload and extract Yago 1 from -
wget http://resources.mpi-inf.mpg.de/yago-naga/yago1_yago2/download/yago1/YAGO1.0.0/yago.zip
Once you've extracted Yago, move the following files from the folder, since they would take a long time to get loaded.
Move these files out to a safe location. We would add them later when required.
a. yago/facts/using/ArticleExtractor.txt
b. yago/facts/type/IsAExtractor.txt
c. yago/facts/foundIn/ArticleExtractor.txt

Then this code allows you to Read YAGO into the Postgres database

Here is how it works:
Open this project in Intellij

Create a run configuration --> Menu --> Run --> Edit Configurations...
Name : ImportYago
Main Class : converters.Converter

Run the project
               
The programs will prompt you for all information needed.

1. First it asks how you want to store Yago.
In our case we are storing it in a database so at the Your choice:> prompt, enter 1

2. Input the location of the facts folder inside the Yago folder, where you unzipped it. For example, the location might be
/home/operator/Downloads/yago/facts  --> edit this as required

3. It would ask to input location where converted Yago should go. This is not important for us since we are importing it
into a db. But we cannot leave it blank, and need to input a directory location that is different from the one entered
 above for the Yago dump. So you may enter something like
 /home/operator/Desktop/  --> edit this as required. This location is not really used later in the program

4. Add deductive closure --> Input yes

5. Add transitive closure --> Input yes

6. Input facts about facts? --> Input yes

7. Next it asks about database details -->
    DB --> Postgres
    username --> postgres (for example)
    password --> 123 (for example)
    host --> localhost
    port --> 5432
    name of Postgres database --> InferenceTest (for example. you would need to create this database manually at the
    postgres prompt)

------------------ Troubleshooting -----------------------------------------

*** In case the program runs into an error, then you would need to delete the yago.ini and drop the table facts from the
database InferenceTest, and then follow these steps again

If you have any problems, questions or suggestions, please send a mail
to aditya@kiotalabs.com

